package com.klaiberdwain.mylibrary.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.Nullable;

public class SQLiteDatabaseHelper extends SQLiteOpenHelper {

    private Context context;
    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "myLibrary";

    public SQLiteDatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String sqlCommandCreateAllBooks = "CREATE TABLE all_books (_ID INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT," +
                "author TEXT, pages INTEGER, image_url TEXT, description TEXT)";

        sqLiteDatabase.execSQL(sqlCommandCreateAllBooks);

        addBooks(sqLiteDatabase);
    }

    private void addBooks(SQLiteDatabase sqLiteDatabase) {

        String sqlInsert1 = "INSERT INTO all_books (title,author,pages,image_url,description) VALUES" +
                "(\"1Q84\", \"Haruki Murakami\",1350,\"https://cdn.shopify.com/s/files/1/0275/7403/products/1Q84.jpg?v=1437447553\"," +
                "\"The novel is a story of how a woman named Aomame begins to notice strange changes occurring in the world. She is quickly caught up in a plot involving Sakigake, a religious cult, and her childhood love," +
                " Tengo, and embarks on a journey to discover what is.\")";

        sqLiteDatabase.execSQL(sqlInsert1);

        String sqlInsert2 = "INSERT INTO all_books (title,author,pages,image_url,description) VALUES" +
                "(\"Iliad\", \"Homer\",1000,\"https://images-na.ssl-images-amazon.com/images/I/51evlbp3ZHL._SX344_BO1,204,203,200_.jpg\"," +
                "\"epic poem in 24 books traditionally attributed to the ancient Greek poet Homer." +
                " It takes the Trojan War as its subject, though the Greek warrior Achilles is its primary focus." +
                " Frontispiece of Homer's The Iliad, translated by John Ogilby, 1660;" +
                " engraving by Wenceslas Hollar\")";

        sqLiteDatabase.execSQL(sqlInsert2);

        String sqlInsert3 = "INSERT INTO all_books (title,author,pages,image_url,description) VALUES" +
                "(\"Holes\", \"Louis Sachar\",233,\"https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1552422497l/38709._SX318_.jpg\"," +
                "\"Holes is a 1998 young adult novel written by Louis Sachar and first published by" +
                " Farrar, Straus and Giroux. The book centers on an unlucky teenage boy named Stanley " +
                "Yelnats, who is sent to Camp Green Lake, a juvenile detention center in a desert in" +
                " Texas, after being falsely accused of theft\")";

        sqLiteDatabase.execSQL(sqlInsert3);

        String sqlInsert4 = "INSERT INTO all_books (title,author,pages,image_url,description) VALUES" +
                "(\"Beyond good and evil\", \"Homer\",250,\"https://prodimage.images-bn.com/pimages/2940155495765_p0_v1_s550x406.jpg\"," +
                "\"Beyond Good and Evil is a comprehensive overview of Nietzsche's mature philosophy. " +
                "The book consists of 296 aphorisms, ranging in length from a few sentences to a few pages. " +
                "These aphorisms are grouped thematically into nine different " +
                "chapters and are bookended by a preface and a poem\")";

        sqLiteDatabase.execSQL(sqlInsert4);

        String sqlInsert5 = "INSERT INTO all_books (title,author,pages,image_url,description) VALUES" +
                "(\"1984\", \"George Orwell\",328,\"https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1348990566l/5470.jpg\"," +
                "\"1984 is a dystopian novella by George Orwell published in 1949, which follows" +
                " the life of Winston Smith, a low ranking member of 'the Party', who is frustrated " +
                "by the omnipresent eyes of the party, and its ominous ruler Big Brother." +
                " 'Big Brother' controls every aspect of people's lives\")";

        sqLiteDatabase.execSQL(sqlInsert5);

        String sqlInsert6 = "INSERT INTO all_books (title,author,pages,image_url,description) VALUES" +
                "(\"Atomic Habits\", \"james clear\",337,\"https://www.samuelthomasdavies.com/wp-content/uploads/2021/01/Atomic-Habits-Summary.jpeg\"," +
                "\"An atomic habit is a regular practice or routine that is not only small and easy" +
                " to do but is also the source of incredible power; a component of the system of " +
                "compound growth. Bad habits repeat themselves again and again not because you don't " +
                "want to change, but because you have the wrong system for change.\")";

        sqLiteDatabase.execSQL(sqlInsert6);

        String sqlInsert7 = "INSERT INTO all_books (title,author,pages,image_url,description) VALUES" +
                "(\"The Alchemist\", \"Paulo Coelho\",660,\"https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1466865542l/18144590._SY475_.jpg\"," +
                "\"The Alchemist is the magical story of Santiago, an Andalusian shepherd boy who" +
                " yearns to travel in search of a worldly treasure as extravagant as any ever found." +
                "From his home in Spain he journeys to the markets of Tangiers and across the " +
                "Egyptian desert to a fateful encounter with the alchemist\")";

        sqLiteDatabase.execSQL(sqlInsert7);
    }


    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void addBook (String tableName, String title,String author,int pages,
                        String image_url, String description){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("title",title);
        contentValues.put("author",author);
        contentValues.put("pages",pages);
        contentValues.put("image_url",image_url);
        contentValues.put("description",description);

        long result = db.insert(tableName,null,contentValues);

        if(result == -1){
            Toast.makeText(context,"Failed",Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(context,"Added successfully!",Toast.LENGTH_SHORT).show();
        }

    }

    public void updateBook (String tableName,String oldTitle, String title,String author,int pages,
                         String image_url, String description){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("title",title);
        contentValues.put("author",author);
        contentValues.put("pages",pages);
        contentValues.put("image_url",image_url);
        contentValues.put("description",description);

        long result = db.update(tableName,contentValues,"title=?",new String[]{oldTitle});

        if(result == -1){
            Toast.makeText(context,"Failed",Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(context,"Added successfully!",Toast.LENGTH_SHORT).show();
        }

    }

    public Cursor readAll (SQLiteDatabase sqLiteDatabase,String tableName){
        Cursor cursor = sqLiteDatabase.query(tableName,null,null,null,
                null,null,null);

        return cursor;
    }

    public void getBook (String tableName,String title){

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(tableName,null,"title=?",new String[] {title},
                null,null,null);

        if(cursor.getCount() >= 1){
            Toast.makeText(context,"Ready add to table",Toast.LENGTH_SHORT).show();
        }

    }

    public void delete (String tableName,String title){

        SQLiteDatabase db = this.getWritableDatabase();

        long result = db.delete(tableName,"title=?",new String[] {title});

        if(result == -1){
            Toast.makeText(context,"Failed to delete",Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(context,"Successfully deleted!",Toast.LENGTH_SHORT).show();
        }
    }

}
