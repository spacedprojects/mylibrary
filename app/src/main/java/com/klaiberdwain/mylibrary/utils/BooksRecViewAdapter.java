package com.klaiberdwain.mylibrary.utils;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.klaiberdwain.mylibrary.R;
import com.klaiberdwain.mylibrary.activities.BookActivity;
import com.klaiberdwain.mylibrary.models.Book;

import java.util.ArrayList;



/**
 *     A recycler view accesses its data using an adapter. Unlike a list view, however, it does not use
 *     any of the built-in Android adapters such as array adapters. Instead, you have to write an adapter
 *     of your own that’s tailored to your data. This includes specifying the type of data, creating
 *     views and binding the data to the views.
 *
 *     The adapter has two main jobs: to create each of the views that are visible within the recycler view,
 *     and to bind each view to a piece of data. This means that the adapter needs to create each card
 *     and bind data to it.
 *
 *     Flow of this class: First the onCreateViewHolder will be called and create a viewHolder class. Then
 *     The onBindVIewHolder will be called and will bind the value that was just created to the recyclerVIew.
 *
 *     The ViewHolder is used to specify which views should be used for each data item. You can think
 *     of it as a holder for the views you want the recycler view to display. In addition to views,
 *     the view holder contains extra information that’s useful to the recycler view, such as its
 *     position in the layout.
 *
 */

public class BooksRecViewAdapter extends RecyclerView.Adapter<BooksRecViewAdapter.ViewHolder>{ //

    private static final String TAG = "BooksRecViewAdapter"; // for logging purposes.

    private ArrayList<Book> books = new ArrayList<>();
    private Context context;
    private Type type;



    public BooksRecViewAdapter(Context context) {
     //   this.books = books; // not sure if a books arrayList should be in the constructor.
        this.context = context;

    }

    /*
        From Tutorial:
        onCreateHolder method is called whenever there is a new object that needs to be populated. That
        means if we have 10 different books this onCreateHolder needs to be called at least 10 times.
        The return type is ViewHolder. First create a view by accessing this layout inflater by using its
        inflate method. Then you create a ViewHolder and pass your view through it. Then return your holder
        as the return type of this method.
        In most cases this is the code that we need to write in order to make this onCreateHolder work.

        From Book:
        The onCreateViewHolder() method gets called when the recycler view requires a new view holder.
        The recycler view calls the method repeatedly when the recycler view is first constructed to build
        the set of view holders that will be displayed on the screen.
        The method takes two parameters: a ViewGroup parent object (the recycler view itself) and an int
        parameter called viewType, which is used if you want to display different kinds of views for
        different items in the list. It returns a view holder object.
         */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_book_rec_view,parent,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    /*
    From Tutorial:
    This onBindViewHolder method is the method that is being called whenever we are binding some value
    to our recyclerView.

    From book:
    You add data by implementing the adapter’s onBindViewHolder() method. This gets called whenever
    the recycler view needs to display data in a view holder. It takes two parameters: the view holder
    the data needs to be bound to, and the position in the data set of the data that needs to be bound.
     */
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called");
        holder.bookName.setText(books.get(position).getTitle());



        holder.bookCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(context, "Selected", Toast.LENGTH_SHORT).show();
                // this OnclickListener is on the cardView bookCard. When click will give Toast message

                Intent intent = new Intent(context, BookActivity.class);
                /*because 'this' is not an activity class but a java class. You have to use 'content' from
                the constructor and not 'this'. You can also pass data through intent. In the next
                line the id of a book object is passed through the intent using the putExtra method.
                name: "bookId" is the key. first give the intent a key and then a value.
                 */
                intent.putExtra("title",books.get(position).getTitle());
                context.startActivity(intent);


            }
        });

        holder.bookCard.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                SQLiteDatabaseHelper sqLiteDatabaseHelper = new SQLiteDatabaseHelper(context);
                Book book = books.get(position);
                switch (type){
                    case ALL_BOOKS:
                        sqLiteDatabaseHelper.delete("all_books",books.get(position).getTitle());
                            notifyDataSetChanged();
                            Toast.makeText(context, book.getTitle()+ " has been deleted", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        Glide.with(context)
                .asBitmap()
                .load(books.get(position).getImageURL())
                .into(holder.bookImage);

    }

    /*
    We also need to tell the adapter how many data items there are. You do this by overriding the
    RecyclerViewAdapter getItemCount() method. This returns an int value, the number of data items. We
    can derive this from the number of books we pass the adapter.
     */
    @Override
    public int getItemCount() {
        return books.size(); //The length of the books array equals the number of data items in the recycler view.
    }

    /*
    This ViewHolder class is responsible for creating every instance of our elements the RecyclerView
     */
    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView bookImage;
        private TextView bookName;
        private CardView bookCard;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            bookImage = itemView.findViewById(R.id.bookImageView);
            bookName = itemView.findViewById(R.id.bookNameTextView);
            bookCard = itemView.findViewById(R.id.bookCard);
            //Define the view to be used for each data item

        }
    }

    public void setBooks(ArrayList<Book> books){
        this.books = books;
        notifyDataSetChanged();
    }

    public void setType(Type type) {
        this.type = type;
    }
}
