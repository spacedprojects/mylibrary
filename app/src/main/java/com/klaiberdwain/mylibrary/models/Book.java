package com.klaiberdwain.mylibrary.models;

import java.util.List;

public class Book {

    private int id;
    private String title;
    private List<String> authorList;
    private String author;
    private int pages;
    private String imageURL;
    private String description;
    private List<String> isbn;

    public Book() {
    }

    public Book(int id, String title, List<String> authorList, int pages, String imageURL, String description, List<String> isbn) {
        this.id = id;
        this.title = title;
        this.authorList = authorList;
        this.pages = pages;
        this.imageURL = imageURL;
        this.description = description;
        this.isbn = isbn;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getAuthorList() {
        return authorList;
    }

    public void setAuthorList(List<String> authorList) {
        this.authorList = authorList;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getIsbn() {
        return isbn;
    }

    public void setIsbn(List<String> isbn) {
        this.isbn = isbn;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", authorList=" + authorList +
                ", pages=" + pages +
                ", imageURL='" + imageURL + '\'' +
                ", description='" + description + '\'' +
                ", isbn=" + isbn +
                '}';
    }
}
