package com.klaiberdwain.mylibrary;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.klaiberdwain.mylibrary.activities.AllBooksActivity;
import com.klaiberdwain.mylibrary.activities.SearchBookActivity;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private Button buttonSeeAllBooks, buttonSearch;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initWidgets();

        setOnClickListeners();

    }

    // intents makes the connection between different activities or between different parts of the application possible
    private void setOnClickListeners(){

        buttonSeeAllBooks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AllBooksActivity.class);
                startActivity(intent);

            }
        });

        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SearchBookActivity.class);
                startActivity(intent);
            }
        });

    }

    private void initWidgets(){

        buttonSeeAllBooks = findViewById(R.id.btnAllBooks);
        buttonSearch = findViewById(R.id.btnSearch);

    }

}
