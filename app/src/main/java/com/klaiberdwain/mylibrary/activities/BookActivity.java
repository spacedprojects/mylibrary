package com.klaiberdwain.mylibrary.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.klaiberdwain.mylibrary.models.Book;
import com.klaiberdwain.mylibrary.fragments.HideOptionsFragment;
import com.klaiberdwain.mylibrary.R;
import com.klaiberdwain.mylibrary.utils.SQLiteDatabaseHelper;

import java.util.ArrayList;

public class BookActivity extends AppCompatActivity {

    private static final String TAG = "BookActivity";

    private TextView bookName, authorName, description, pageNumber;
    private ImageView bookImage;
    private Button btnTrackTime;

    private Book incomingBook;
    private SQLiteDatabaseHelper databaseHelper;

    private ArrayList<Book> books = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        databaseHelper = new SQLiteDatabaseHelper(this);

        initWidgets();

        getAllBooks();

        loadFragment();

        Intent intent = getIntent();
        String title = intent.getStringExtra("title");


        for(Book b:books){
            if(b.getTitle().equals(title)){
                incomingBook = b;
                bookName.setText(b.getTitle());
                authorName.setText(b.getAuthor());
                description.setText(b.getDescription());
                pageNumber.setText("Pages: " + b.getPages());

                Glide.with(this).
                        asBitmap()
                        .load(b.getImageURL())
                        .into(bookImage);
            }
        }


        btnTrackTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BookActivity.this, TrackTimeActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu,menu);
        return true;
    }

    private void loadFragment() {

        // your can pass different transactions with this transaction
        // your can add, remove or replace fragments.
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        //create fragment and pass it to the transaction
        HideOptionsFragment hideOptionsFragment = new HideOptionsFragment();
        // replace the container with this fragment
        fragmentTransaction.replace(R.id.container,hideOptionsFragment);
        fragmentTransaction.commit();

    }

    public void hideButtons(){
        btnTrackTime.setVisibility(View.GONE);
    }

    public void showButtons(){
        btnTrackTime.setVisibility(View.VISIBLE);
    }


    public void getAllBooks(){

        SQLiteDatabase database = databaseHelper.getReadableDatabase();

        Cursor cursor = databaseHelper.readAll(database,"all_books");

        if(cursor.getCount() == 0){
            Toast.makeText(this,"No Data",Toast.LENGTH_LONG).show();
        } else {
            while (cursor.moveToNext()){
                Book book = new Book();
                book.setId(cursor.getInt(0));
                book.setTitle(cursor.getString(1));
                book.setAuthor(cursor.getString(2));
                book.setPages(cursor.getInt(3));
                book.setImageURL(cursor.getString(4));
                book.setDescription(cursor.getString(5));

                books.add(book);
            }
        }

    }

    private void initWidgets(){

        bookName = findViewById(R.id.bookNameTxtView);
        authorName = findViewById(R.id.authorNameTxtView);
        description = findViewById(R.id.bookDescriptionTxtView);
        pageNumber = findViewById(R.id.bookPages);

        bookImage = findViewById(R.id.bookImage);

        btnTrackTime = findViewById(R.id.trackTimeBtn);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                super.onBackPressed();
                break;
            case R.id.item_update_book:
                goToUpdateBookActivity();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void goToUpdateBookActivity() {
        Intent intent = new Intent(BookActivity.this, UpdateActivity.class);
        intent.putExtra("title",incomingBook.getTitle());
        intent.putExtra("author",incomingBook.getAuthor());
        intent.putExtra("pages",incomingBook.getPages());
        intent.putExtra("imageUrl",incomingBook.getImageURL());
        intent.putExtra("description",incomingBook.getDescription());
        startActivity(intent);
    }

}
