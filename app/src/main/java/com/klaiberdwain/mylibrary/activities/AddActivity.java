package com.klaiberdwain.mylibrary.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.klaiberdwain.mylibrary.R;
import com.klaiberdwain.mylibrary.utils.SQLiteDatabaseHelper;

public class AddActivity extends AppCompatActivity {

    EditText title,author,pages,description,imageUrl;
    Button add_button;

    private String bookTitle;
    private String bookAuthor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        initWidgets();

        Intent intent = getIntent();

        if(intent != null){
            bookTitle = intent.getStringExtra("title");
            bookAuthor = intent.getStringExtra("author");

            title.setText(bookTitle);
            author.setText(bookAuthor);
        }


        add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SQLiteDatabaseHelper sqLiteDatabaseHelper = new SQLiteDatabaseHelper(AddActivity.this);
                sqLiteDatabaseHelper.addBook("all_books",
                        title.getText().toString().trim(),
                        author.getText().toString().trim(),
                        Integer.valueOf(pages.getText().toString().trim()),
                        imageUrl.getText().toString().trim(),
                        description.getText().toString().trim());
            }
        });


    }

    private void initWidgets(){
        title = findViewById(R.id.editTextTitle);
        author = findViewById(R.id.editTextAuthor);
        pages = findViewById(R.id.editTextPages);
        description = findViewById(R.id.editTextDescription);
        imageUrl = findViewById(R.id.editTextImageUrl);
        add_button = findViewById(R.id.addBookBtn);

    }
}