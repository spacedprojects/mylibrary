package com.klaiberdwain.mylibrary.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.klaiberdwain.mylibrary.models.Book;
import com.klaiberdwain.mylibrary.fragments.BookSearchResultFragment;
import com.klaiberdwain.mylibrary.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SearchBookActivity extends AppCompatActivity {

    private static final String TAG = "WebViewActivity";

    private Button searchBtn;
    private EditText editText;

    private String searchValue;

    private Book book;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_book);

        searchBtn = findViewById(R.id.searchBookBtn);
        editText = findViewById(R.id.editTextSearchBook);

        book = new Book();

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchValue = editText.getText().toString();
                sendRequest();
            }
        });

    }

    private void sendRequest() {

        String url = "https://openlibrary.org/search.json?title=" + searchValue;

        // create StringRequest
        // specify the request type
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                // this response message will be executed whenever we receive a response. For example from a server

                // cast json file to Post object

                List<String> isbnList = new ArrayList<>();
                List<String> authorList = new ArrayList<>();

                try {
                    JSONArray jsonArray = response.getJSONArray("docs");

                    for(int i = 0; i < 1 ;i++){
                        JSONObject jsonBookObject = jsonArray.getJSONObject(i);

                        String title = jsonBookObject.getString("title");
                        JSONArray jsonArrayISBN = jsonBookObject.getJSONArray("isbn");
                        JSONArray jsonArrayAuthors = jsonBookObject.getJSONArray("author_name");

                        for(int j = 0; j < 5 ;j++){
                            String isbn = jsonArrayISBN.getString(j);
                            isbnList.add(isbn);
                        }

                        for(int j = 0; j < jsonArrayAuthors.length();j++){
                            String author = jsonArrayAuthors.getString(j);
                            authorList.add(author);
                        }

                        book.setTitle(title);
                        book.setAuthorList(authorList);
                        book.setIsbn(isbnList);
                    }

                    Toast.makeText(SearchBookActivity.this,book.getTitle(),Toast.LENGTH_SHORT).show();
                    System.out.println(book);

                    if(book != null){
                        loadFragment();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //this will be executed whenever an error occurs. like a 404 error
                error.printStackTrace();
            }
        });


        // create a requestQueue to pass the stringRequest
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);
        requestQueue.start();
    }

    private void loadFragment() {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        BookSearchResultFragment bookSearchResultFragment = new BookSearchResultFragment();
        fragmentTransaction.replace(R.id.bookSearchResultContainer,bookSearchResultFragment);
        fragmentTransaction.commit();
    }

    public Book getBook() {
        return book;
    }
}