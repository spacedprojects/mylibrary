package com.klaiberdwain.mylibrary.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.Chronometer;

import com.klaiberdwain.mylibrary.R;
import com.klaiberdwain.mylibrary.services.TrackTimeService;

import java.util.Objects;

public class TrackTimeActivity extends AppCompatActivity {

    private static final String TAG = "TrackTimeActivity";

    private Chronometer chronometer;
    private static long pauseOffset;
    private boolean running;

    private Intent forService;

    DataBroadcastReceiver dataBroadcastReceiver;
    IntentFilter dataIntentFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_time);

        forService = new Intent(TrackTimeActivity.this, TrackTimeService.class);

        dataBroadcastReceiver = new DataBroadcastReceiver();
        dataIntentFilter = new IntentFilter("DATA_TRANSFER");
        registerReceiver(dataBroadcastReceiver,dataIntentFilter);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel channel = new NotificationChannel("timerTracker","TimerTracker", NotificationManager.IMPORTANCE_LOW);
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }

        chronometer = findViewById(R.id.chronometer);
        chronometer.setFormat("Time: %s");

        if(isMyServiceRunning(TrackTimeService.class)){
            stopService(forService);

            SharedPreferences sh = getSharedPreferences("MySharedPref", MODE_PRIVATE);

            pauseOffset = sh.getLong("pauseOffset",0);

            chronometer.setBase(SystemClock.elapsedRealtime() - pauseOffset);
            System.out.println(pauseOffset);

        }
    }



    @Override
    protected void onPostResume() {

        super.onPostResume();
    }

    public void startChronometer(View v) {
        startService();
        if (!running) {
            chronometer.setBase(SystemClock.elapsedRealtime() - pauseOffset);
            System.out.println(pauseOffset);
            chronometer.start();
            running = true;
        }
    }
    public void pauseChronometer(View v) {
        if (running) {
            chronometer.stop();
            pauseOffset = SystemClock.elapsedRealtime() - chronometer.getBase();
            System.out.println(pauseOffset);
            running = false;
        }
    }
    public void resetChronometer(View v) {
        chronometer.setBase(SystemClock.elapsedRealtime());
        System.out.println(pauseOffset);
        pauseOffset = 0;
    }

    public void stopChronometer(View v) {
        chronometer.stop();
        pauseOffset = SystemClock.elapsedRealtime() - chronometer.getBase();
        System.out.println(pauseOffset);
        running = false;
        stopService(forService);
    }

    private void startService(){

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            // run service
            startForegroundService(forService);
        } else {
            startService(forService);
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public class DataBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(Objects.equals(intent.getAction(), "DATA_TRANSFER")){
                System.out.println("received intent from service");
                pauseOffset = intent.getLongExtra("pauseOffset",0);
            }
        }
    }
}