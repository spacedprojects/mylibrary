package com.klaiberdwain.mylibrary.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.klaiberdwain.mylibrary.R;
import com.klaiberdwain.mylibrary.utils.SQLiteDatabaseHelper;

public class UpdateActivity extends AppCompatActivity {

    EditText title,author,pages,description,imageUrl;
    Button update_button;

    String bookTitle,bookAuthor,bookDescription,bookImageUrl;
    int id, bookPages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        initWidgets();

        getAndSetBookInfo();

        update_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newBookTitle = title.getText().toString().trim();
                SQLiteDatabaseHelper sqLiteDatabaseHelper = new SQLiteDatabaseHelper(UpdateActivity.this);
                sqLiteDatabaseHelper.updateBook("all_books",bookTitle,newBookTitle,bookAuthor,
                        bookPages,bookImageUrl,bookDescription);
            }
        });

    }

    private void getAndSetBookInfo() {
        Intent intent = getIntent();
        bookTitle = intent.getStringExtra("title");
        bookAuthor = intent.getStringExtra("author");
        bookPages = intent.getIntExtra("pages",0);
        bookDescription = intent.getStringExtra("description");
        bookImageUrl = intent.getStringExtra("imageUrl");

        title.setText(bookTitle);
        author.setText(bookAuthor);
        pages.setText(String.valueOf(bookPages));
        description.setText(bookDescription);
        imageUrl.setText(bookImageUrl);

    }

    private void initWidgets(){
        title = findViewById(R.id.editTextUpdateTitle);
        author = findViewById(R.id.editTextUpdateAuthor);
        pages = findViewById(R.id.editTextUpdatePages);
        description = findViewById(R.id.editTextUpdateDescription);
        imageUrl = findViewById(R.id.editTextUpdateImageUrl);
        update_button = findViewById(R.id.updateBookBtn);

    }

}