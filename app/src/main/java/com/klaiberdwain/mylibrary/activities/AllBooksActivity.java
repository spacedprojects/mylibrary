package com.klaiberdwain.mylibrary.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.klaiberdwain.mylibrary.models.Book;
import com.klaiberdwain.mylibrary.utils.BooksRecViewAdapter;
import com.klaiberdwain.mylibrary.R;
import com.klaiberdwain.mylibrary.utils.SQLiteDatabaseHelper;
import com.klaiberdwain.mylibrary.utils.Type;

import java.util.ArrayList;

public class AllBooksActivity extends AppCompatActivity {

    private static final String TAG = "AllBooksActivity"; // logs through out the project. type : logt for the TAG

    private RecyclerView booksRecyclerView;
    private FloatingActionButton add_button;

    private BooksRecViewAdapter adapter;

    ArrayList<Book> books;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_books);

        DatabaseAsyncTask databaseAsyncTask = new DatabaseAsyncTask();
        databaseAsyncTask.execute();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        books =  new ArrayList<>();

        Log.d(TAG, "onCreate: started"); // where every you want to type in a log. type: logd

        booksRecyclerView = findViewById(R.id.BooksRecView);
        add_button = findViewById(R.id.floatingActionButton);

        adapter = new BooksRecViewAdapter(this);
        booksRecyclerView.setAdapter(adapter);
        booksRecyclerView.setLayoutManager(new GridLayoutManager(this,2));

        adapter.setType(Type.ALL_BOOKS);

        add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllBooksActivity.this, AddActivity.class);
                startActivity(intent);
            }
        });



    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                super.onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class DatabaseAsyncTask extends AsyncTask<Void,Void,String> {

        private SQLiteDatabaseHelper databaseHelper;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            databaseHelper = new SQLiteDatabaseHelper(AllBooksActivity.this);
        }

        @Override
        protected String doInBackground(Void... voids) {

            SQLiteDatabase database = databaseHelper.getReadableDatabase();

            Cursor cursor = databaseHelper.readAll(database,"all_books");

            if(cursor.getCount() == 0){
                Toast.makeText(AllBooksActivity.this,"No Data",Toast.LENGTH_LONG).show();
            } else {
                while (cursor.moveToNext()){
                    Book book = new Book();
                    book.setId(cursor.getInt(0));
                    book.setTitle(cursor.getString(1));
                    book.setAuthor(cursor.getString(2));
                    book.setPages(cursor.getInt(3));
                    book.setImageURL(cursor.getString(4));
                    book.setDescription(cursor.getString(5));

                    books.add(book);
                }
            }
            cursor.close();
            database.close();

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            adapter.setBooks(books);
        }
    }
}
