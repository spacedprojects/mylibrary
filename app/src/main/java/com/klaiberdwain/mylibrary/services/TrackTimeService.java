package com.klaiberdwain.mylibrary.services;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.IBinder;
import android.os.SystemClock;
import android.widget.Chronometer;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

public class TrackTimeService extends Service {

    private Chronometer chronometer;

    private long pauseOffset;

    SharedPreferences preferences;

    @Override
    public void onCreate() {
        super.onCreate();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"timerTracker")
                    .setContentText("timer is running")
                    .setContentTitle("My library");

            startForeground(101,builder.build());
        }

        preferences  = getSharedPreferences("sharedPref", MODE_PRIVATE);

        chronometer = new Chronometer(TrackTimeService.this);

        chronometer.setBase(SystemClock.elapsedRealtime() - pauseOffset);
        chronometer.start();
    }

    @Override
    public void onDestroy() {
        SharedPreferences.Editor myEdit = preferences.edit();
        myEdit.putLong("pauseOffset",pauseOffset);
        myEdit.apply();
        Intent i = new Intent("DATA_TRANSFER");
        chronometer.stop();
        pauseOffset = SystemClock.elapsedRealtime() - chronometer.getBase();
        i.putExtra("pauseOffset",pauseOffset);
        sendBroadcast(i);
        System.out.println("from onDestroy " + pauseOffset);
        super.onDestroy();
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
