package com.klaiberdwain.mylibrary.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.klaiberdwain.mylibrary.models.Book;
import com.klaiberdwain.mylibrary.R;
import com.klaiberdwain.mylibrary.activities.AddActivity;
import com.klaiberdwain.mylibrary.activities.SearchBookActivity;

public class BookSearchResultFragment extends Fragment {

    private static final String TAG = "BookSearchResultFragmen";

    private TextView titleTextView,authorTextView;

    private Button addToLibrary;

    public BookSearchResultFragment() {
    }

    //in order to create the layout you need to override a method called create view
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //inflate view file
        View view = inflater.inflate(R.layout.fragment_book_search_result, container, false);

        titleTextView = (TextView) view.findViewById(R.id.textBookTitle);
        authorTextView = (TextView) view.findViewById(R.id.textBookAuthor);

        final Book book = ((SearchBookActivity)getActivity()).getBook();

        titleTextView.setText("Title: " + book.getTitle());
        authorTextView.setText("Author: " + book.getAuthorList().get(0));

        addToLibrary = (Button) view.findViewById(R.id.addLibraryBtn);

        addToLibrary.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), AddActivity.class);
                intent.putExtra("title",book.getTitle());
                intent.putExtra("author",book.getAuthorList().get(0));
                startActivity(intent);

            }
        });

        return view;
    }
}
