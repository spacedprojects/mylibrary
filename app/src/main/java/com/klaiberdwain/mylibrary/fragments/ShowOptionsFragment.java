package com.klaiberdwain.mylibrary.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.klaiberdwain.mylibrary.R;
import com.klaiberdwain.mylibrary.activities.BookActivity;
import com.klaiberdwain.mylibrary.fragments.HideOptionsFragment;

public class ShowOptionsFragment extends Fragment {

    private static final String TAG = "ShowOptionsFragment";

    private Button showOptionBtn;

    public ShowOptionsFragment() {
    }

    //in order to create the layout you need to override a method called create view
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //inflate view file
        View view = inflater.inflate(R.layout.fragment_show_option_btn, container, false);

        showOptionBtn = (Button) view.findViewById(R.id.showOptionsButton);

        showOptionBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                ((BookActivity)getActivity()).showButtons();
                HideOptionsFragment hideOptionsFragment = new HideOptionsFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container, hideOptionsFragment);
                fragmentTransaction.commit();
            }
        });

        return view;
    }
}
