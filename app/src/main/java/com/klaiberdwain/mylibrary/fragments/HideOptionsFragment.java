 package com.klaiberdwain.mylibrary.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.klaiberdwain.mylibrary.R;
import com.klaiberdwain.mylibrary.activities.BookActivity;

 public class HideOptionsFragment extends Fragment {

    private static final String TAG = "HideOptionsFragment";

    private Button hideOptionBtn;
    
    public HideOptionsFragment() {
    }

    //in order to create the layout you need to override a method called create view
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //inflate view file
        View view = inflater.inflate(R.layout.fragment_hide_option_btn, container, false);

        hideOptionBtn = (Button) view.findViewById(R.id.hideOptionsButton);

        hideOptionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((BookActivity)getActivity()).hideButtons();
                ShowOptionsFragment showOptionsFragment = new ShowOptionsFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container, showOptionsFragment);

                fragmentTransaction.commit();
            }
        });

        return view;
    }
}
